from django.db import models
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    foto = models.ImageField(
        upload_to='users/avatars/',
        blank=True,
        null=True
    )

    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username