from .models import Perfil
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    
    foto = serializers.ImageField(
        max_length=None,
        use_url=True
    )

    class Meta:
        model = Perfil
        fields = ('foto', )

    def create(self, validated_data):
        return Perfil.objects.create(**validated_data)