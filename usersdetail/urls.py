from django.urls import path, include

from usersdetail import views

urlpatterns = [

    path('upload/', views.CandidateListView.as_view()),
    path('upload/<int:pk>', views.CandidateListView.as_view()),

]