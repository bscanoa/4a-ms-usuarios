from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.views import APIView
from .models import Perfil
from .serializer import UserSerializer

class CandidateListView(APIView):

    def get(self, request, format=None):
        candidates = Perfil.objects.all()
        serializer = UserSerializer(candidates, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(
                foto=request.data.get('foto')
            )
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk, format=None):
        perfil = Perfil.objects.get(user=pk) 
        serializer = UserSerializer(perfil, data=request.data)
        if serializer.is_valid():
            serializer.save(
                foto=request.data.get('foto')
            )
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)